{ pkgs, ... }:
rec {
  "qutebrowser-launch" = pkgs.writeShellApplication {
    name = "qutebrowser-launch";
    runtimeInputs = [ pkgs.qutebrowser ];
    text = builtins.readFile ./scripts/qutebrowser_session_wrapper.sh;
  };
  "rofi-qutebrowser" = pkgs.writeShellApplication {
    name = "rofi-qutebrowser";
    runtimeInputs = [ pkgs.rofi ];
    text = ''
      XDG_DATA_HOME="$HOME/.local/share/"
      SESSIONS=$(find "$XDG_DATA_HOME/qutebrowser/sessions" -name '*.yml' -type f -printf "%f\n" |
            sed -r -e 's/\.[^\.]*//' -e '/^$/d' -e '/^_/d')

      SESSION=$(rofi -dmenu -p "Session: " <<< "$SESSIONS")

      test -z "$SESSION" && exit 1

      # Just quit if we don't find it -- TODO: Write a seperate script for making new sessions
      find "$XDG_DATA_HOME/qutebrowser/sessions/$SESSION.yml" -type f || exit 2 #|| echo "$NEW_SESSION" > "$XDG_DATA_HOME/qutebrowser/sessions/$SESSION.yml"

      exec ${qutebrowser-launch}/bin/qutebrowser-launch --target window -r "$SESSION"
    '';
  };
  "qutebrowser-current-session" = pkgs.writeShellApplication {
    name = "qutebrowser-current-session";
    runtimeInputs = [ pkgs.qutebrowser ];
    text = ''
      # open the default browser if we got nothing
      if [ -z "''${1-}" ]; then
        exec qutebrowser
      fi

      # Get the name of the first active session found
      # shellcheck disable=SC2009
      SESSION=$(ps aux | grep -E 'run/user/[[:digit:]]+/qutebrowser' | head -1 | awk '{for (i=1;i<=NF;i++){if ($i ~ /^\/run\/user/) {print $i}}}')

      # if there isn't one just launch an empty browser
      if [ -z "$SESSION" ]; then
        exec qutebrowser "$1"
      else
        exec qutebrowser --basedir "$SESSION" "$1"
      fi
    '';
  };
  "hyprfreeze" = pkgs.stdenv.mkDerivation {
    pname = "hyprfreeze";
    version = "v1.1.0";
    buildInputs = [ pkgs.psmisc ];
    src = builtins.fetchGit {
      url = "https://github.com/Zerodya/hyprfreeze.git";
      ref = "v1.1.0";
    };
  };
    /*
    "ha-office-ctl" = pkgs.buildPythonPackage rec {
      pname = "ha-office-ctl";
      version = "0.0.1";
      propogatedBuildInputs = with pkgs.python310Packages; [ requests ];

      src =  pkgs.writeText "${pname}.py" ''
        import requests
        import sys

        if sys.argv[1] == "on":
            scene = "scene.office_on"
        elif sys.argv[1] == "off":
            scene = "scene.office_off"
        else:
            print("Expected exactly 1 argument of either: on, off", file=sys.stderr)
            sys.exit(1)

        # Read the api token from the store
        #with open('{age.secrets."home-assistant".path}') as f:
        #  TOKEN = f.read()

        url = "http://homeassistant.cw.moog.games:8123/api/services/scene/turn_on"
        headers = {"Authorization": f"Bearer {TOKEN}"}
        data = {"entity_id": scene}

        resp = requests.post(url, headers=headers, json=data)
      '';
    };*/
}