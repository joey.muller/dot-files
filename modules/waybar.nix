{ pkgs, config, lib, ... }:
with lib;
let 
  cfg = config.modules.waybar;
  gpu-info-script = lib.getExe (pkgs.writeShellApplication {
    name = "gpu-info-script";
    runtimeInputs = with pkgs; [ jq amdgpu_top coreutils ];
    text = ''
    DATA=$(amdgpu_top -J -n 1 -s 1 | jq '.devices[0]')
    VRAM_USED=$(echo "$DATA" | jq '.VRAM."Total VRAM Usage".value' | numfmt --from-unit Mi --to iec --format "%3.1f")
    VRAM_TOTAL=$(echo "$DATA" | jq '.VRAM."Total VRAM".value' | numfmt --from-unit Mi --to iec --format "%3.1f")
    GPU_USAGE=$(echo "$DATA" | jq '.gpu_activity.GFX.value')
    GPU_TEMP=$(echo "$DATA" | jq '.Sensors."Edge Temperature".value')

    echo " 󰮄 $GPU_USAGE% | $GPU_TEMP°C 󰘚 $VRAM_USED/$VRAM_TOTAL "
    '';
  });
in
{
  options.modules.waybar = {
    enable = mkEnableOption "Waybar status bar";
  };

  config = mkIf cfg.enable {
    programs.waybar = {
      enable = true;
      systemd.enable = true;

      settings = [{
        layer = "top";
        position = "top";
        height = 26;

        modules-left = [
          "custom/power"
          "group/systemstats"
        ];
        modules-center = [
          "hyprland/workspaces"
        ];
        modules-right = [
          "network"
          "wireplumber"
          "clock"
        ];
        "group/systemstats" = {
          orientation = "horizontal";
          modules = [
            "cpu"
            "temperature"
            "memory"
            "custom/gpu"
            "disk"
          ];
        };

        # font icons from https://www.nerdfonts.com/cheat-sheet
        clock = {
          format = " {:%I:%M}";
        };

        wireplumber = {
          format = "{icon} {volume}";
          format-bluetooth = "";
          format-muted = "  ---";
          format-icons = {
            default = [ "" "" ];
          };
          tooltip = false;
          on-click = "pavucontrol";
          on-click-right = "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
        };

        cpu = {
          interval = 1;
          format = " {usage}% |";
          tooltip = false;
        };

        memory = {
          interval = 5;
          format = " {used:2.1f}/{total:2.1f}G";
          tooltip = false;
        };

        disk = {
          interval = 15;
          format = "󰋊 {percentage_used}%";
          tooltip = false;
        };
        
        "custom/gpu" = {
          exec = gpu-info-script;
          tooltip = false;
          interval = 3;
        };

        network = {
          interval = 3;
          interface = "lan";
          format = "󰲝 {ipaddr}/{cidr}";
          tooltip = false;
        };

        temperature = {
          thermal-zone = 1;
          hwmon-path = "/sys/class/hwmon/hwmon3/temp1_input";
          format = "{temperatureC}°C";
          tooltip = false;
        };

        "hyprland/workspaces" = {
          format = "{name}";
          on-scroll-up = "hyprctl dispatch workspace e+1";
          on-scroll-down = "hyprctl dispatch workspace e-1";
        };

        "custom/power" = {
          format = "";
          tooltip = false;
          on-click = "wlogout";
        };
      }];
      style = ./waybar/style.css;
    };
  };
}


