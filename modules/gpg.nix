{ pkgs, config, lib, ... }:
with lib;
let cfg = config.modules.gpg;
in
{
  options.modules.gpg.enable = mkEnableOption "gpg";

  config = mkIf cfg.enable {
    programs.gpg = {
      enable = true;

      settings = {
        no-comments = true;
        require-cross-certification = true;
        default-key = [
          # Signing
          "C371 3D5B D254 D1B4 62E3  4E50 0ECE 373E CC28 0B4D"
          # Encryption
          "0376 8688 64A5 EDB3 0378  CB68 0BF6 20BB A113 948A"
        ];
      };
    };

    services.gpg-agent = {
      enable = true;

      # let the agent keeps this cached "forever"
      defaultCacheTtl = 34560000;
      maxCacheTtl = 34560000;
      defaultCacheTtlSsh = 34560000;
      maxCacheTtlSsh = 34560000;

      enableSshSupport = true;
      sshKeys = [
        "70BABA448B21284768BC4E69F14F47504453ACCF" # rsa
        "0354EAD98D06D482865207B459BFA33B55679619" # ed
      ];
    };
  };
}
