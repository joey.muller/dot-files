{ config, lib, scripts, ... }:
with lib;
let cfg = config.modules.hyprland;
in
{
  options.modules.hyprland = {
    enable = mkEnableOption "Hyprland Window Manager";
    monitors = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = ''
        List of monitors to use for Hyprland.
      '';
    };
  };

  config = mkIf cfg.enable {
    wayland.windowManager.hyprland = {
      enable = true;
      xwayland.enable = true;

      plugins = [ ];

      settings = {
        monitor = [ ",preferred,auto,auto,bitdepth,10" ] ++ cfg.monitors;

        input = {
          kb_layout = "
            us ";
          kb_options = "caps:escape";
          numlock_by_default = true;

          follow_mouse = 1;
          sensitivity = 0;

          repeat_rate = 35;
          repeat_delay = 200;
        };

        general = {
          gaps_in = 2;
          gaps_out = "0,8,8,8";
          border_size = 1;

          "col.active_border" = "rgba(1aff1aee)";
          "col.inactive_border" = "rgba(595959aa)";

          layout = "dwindle";
        };

        decoration = {
          rounding = 4;

          drop_shadow = "no";
          shadow_range = 4;
          shadow_render_power = 3;
          "col.shadow" = "rgba(1a1a1aee)";
        };

        animations = {
          enabled = "yes";

          bezier = "bez, 0.05, 0.9, 0.1, 1.05";

          animation = [
            "windows, 1, 7, bez"
            "windowsOut, 1, 7, default, popin 80%"
            "border, 1, 10, default"
            "fade, 1, 7, default"
            "workspaces, 1, 6, default"
          ];
        };

        dwindle = {
          pseudotile = "no";
          preserve_split = "yes";
        };

        master = {
          new_is_master = true;
        };

        gestures = {
          workspace_swipe = "off";
        };

        "$mod" = "SUPER";

        bind = [
          # Move window focus
          "$mod, J, movefocus, l"
          "$mod, L, movefocus, r"
          "$mod, I, movefocus, u"
          "$mod, K, movefocus, d"

          # Move window
          "$mod SHIFT, J, movewindow, l"
          "$mod SHIFT, L, movewindow, r"
          "$mod SHIFT, I, movewindow, u"
          "$mod SHIFT, K, movewindow, d"

          # Switch workspaces
          "$mod, 1, workspace, 1"
          "$mod, 2, workspace, 2"
          "$mod, 3, workspace, 3"
          "$mod, 4, workspace, 4"
          "$mod, 5, workspace, 5"
          "$mod, 6, workspace, 6"
          "$mod, 7, workspace, 7"
          "$mod, 8, workspace, 8"
          "$mod, 9, workspace, 9"
          "$mod, 0, workspace, 10"

          # Move active windo to workspace
          "$mod SHIFT, 1, movetoworkspace, 1"
          "$mod SHIFT, 2, movetoworkspace, 2"
          "$mod SHIFT, 3, movetoworkspace, 3"
          "$mod SHIFT, 4, movetoworkspace, 4"
          "$mod SHIFT, 5, movetoworkspace, 5"
          "$mod SHIFT, 6, movetoworkspace, 6"
          "$mod SHIFT, 7, movetoworkspace, 7"
          "$mod SHIFT, 8, movetoworkspace, 8"
          "$mod SHIFT, 9, movetoworkspace, 9"
          "$mod SHIFT, 0, movetoworkspace, 10"

          # Utility binds
          "$mod SHIFT, Q, killactive"
          "$mod, S, togglefloating"
          "$mod, F, fullscreen"
          ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 3%+"
          ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 3%-"
          #", PAUSE, exec, ${scripts.hyprfreeze} -a"

          # Exec binds
          "$mod, D, exec, wofi --show drun"
          "$mod SHIFT, D, exec, ~/scripts/dmenu_steam"

          "$mod SHIFT, space, exec, alacritty"
          "$mod CONTROL, space, exec, ${scripts.rofi-qutebrowser}/bin/rofi-qutebrowser"

          "$mod, up, exec, ~/scripts/home_assistant/office_lights.py on"
          "$mod, down, exec, ~/scripts/home_assistant/office_lights.py off"
        ];

        bindm = [
          # Move/resize with mouse
          "$mod, mouse:272, movewindow"
          "$mod, mouse:273, resizewindow"
        ];
      };
    };
  };
}

